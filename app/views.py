from rest_framework.views import APIView
from .serializers import FormSerializer,TranslationSerializer
from .models import Country,Connected
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework import status
from django.http import Http404 , JsonResponse
from googletrans import Translator
import  uuid
from django.views.generic import DetailView
# Create your views here.
class IndexView(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'index.html'
    def get(self,request,*args,**kwargs):
        country_details = Country.objects.all()
        #for i in country_details:
            #if len(i.country_names) == 5:
                #return Response({'cu':i})
        return Response({'cd':country_details})

class FormRegistration(APIView):
    def post(self,request,*args,**kwargs):
        form_registartion = FormSerializer(data=request.data)
        if form_registartion.is_valid():
            user_form = form_registartion.save()
            if user_form:
                return Response({'msg':'form submission successful'},status=status.HTTP_201_CREATED)
            else:
                return Response({"msg":"Internal error"},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"msg":"error forbidden"},status=status.HTTP_403_FORBIDDEN)


class TranslationView(APIView):
    def post(self,request,*args,**kwargs):
        translators = TranslationSerializer(data=request.data)
        if translators.is_valid():
            transform = translators.save()
            if transform:
                return  Response({"msg":"success"}, status=status.HTTP_201_CREATED)
            else:
                return Response({"msg":"Internal Error"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({"msg":"error forbidden"},status=status.HTTP_403_FORBIDDEN)



class TranslationDetailView(APIView):
    def get_object(self,request,pk,*args,**kwargs):
        try:
            return Translation.objects.get(pk=pk)
        except Translation.DoesNotExist:
            raise Http404
    def patch(self,request,pk,**kwargs):
        translation_data = self.get_object(pk)
        serializer = TranslationSerializer(translation_data, data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return  Response(serializer.data)
        return  Response ({"msg":"error forbidden"},status=status.HTTP_403_FORBIDDEN)
    def get(self,request,*args,**kwargs):
        all_translation_data = Translation.objects.all()
        return Response({"trd":all_translation_data})
    def get(self,request,*args,**kwargs):
        language_data = Transation.objects.all().get(country_name)
        return Response ({"lang":language_data})
class DetailView(DetailView):
    model = Country
    template_name = 'detail.html'
class ConnectedView(DetailView):
    model = Connected
    template_name = 'detail.html'
