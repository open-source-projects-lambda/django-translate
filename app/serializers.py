from rest_framework import serializers
from app.models import Language,Country,Requirement,FormDetails,Translation

class LanguageSerializer (serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'
        def create(self,validated_data):
            return super(LanguageSerializers,self).create(validated_data)
class ReqSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Requirement
        fields = '__all__'
        def create(self,validated_data):
            return super(ReqSerializer,self).create(validated_data)
class CountrySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'
        def create(self,validated_data):
            return super(CountrySerializer,self).create(validated_data)


class FormSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FormDetails
        fields = '__all__'

        def create (self, validated_data):
            return super(FormSerializer,self).create(validated_data)
class TranslationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Translation
        fields = '__all__'
        def create(self,validated_data):
            return super(TranslationSerializer,self).create(validated_data)
