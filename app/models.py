from django.db import models
import uuid
from googletrans import Translator
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

# Create your models here.
class Language(models.Model):
    language_name = models.CharField(max_length=50)
    language_code = models.CharField(max_length=20)

    def __str__(self):
        return f"{self.language_name}"
class Requirement(models.Model):
    requirement_names = models.CharField(max_length=50)
    requirement_code = models.IntegerField(auto_created=True)

    def __str__(self):
        return f"{self.requirement_names}"

class Country(models.Model):
    language_name = models.CharField(max_length=50,default="SPANISH")
    language_code = models.CharField(max_length=20,default="IT")
    country_names = models.CharField(max_length=50,default="SPANISH")
    country_codes = models.CharField(max_length = 40,default="IT")
    cookie_set =  models.CharField(max_length=100,default="vvf")
    connected_language_code = models.CharField(max_length=20,default="IT",null=True,blank=True)
    connected_country_names = models.CharField(max_length=50,default="SPANISH",null=True,blank=True)
    connected_country_codes = models.CharField(max_length = 40,default="IT",null=True,blank=True)
    connected_cookie_set =  models.CharField(max_length=100,default="vvf",null=True,blank=True)
    connected_language_name = models.CharField(max_length=100,null=True,blank=True,default="SPANISH")
    connected_text =  models.TextField(default="Add text",null=True,blank=True)
    connected_translation_language =  models.CharField(max_length=50,null=True,blank=True,default="English")
    connected_translation_language_second = models.CharField(max_length=50,null=True,blank=True,default="Us-engilsh")
    connected_translation_language_third = models.CharField(max_length=50,null=True,blank=True,default="indian-english")
    connected_t_code_fi = models.CharField(max_length=50,null=True,blank=True,default="ES1")
    connected_t_code_tw = models.CharField(max_length=50,null=True,blank=True,default="ES2")
    connected_t_code_th = models.CharField(max_length=50,null=True,blank=True,default="ES3")
    text = models.TextField(default="Add text",null=True,blank=True)
    translation_language = models.CharField(max_length=50,null=True,blank=True,default="English")
    translation_language_second = models.CharField(max_length=50,null=True,blank=True,default="Us-engilsh")
    translation_language_third = models.CharField(max_length=50,null=True,blank=True,default="indian-english")
    t_code_fi = models.CharField(max_length=50,null=True,blank=True,default="ES1")
    t_code_tw = models.CharField(max_length=50,null=True,blank=True,default="ES2")
    t_code_th = models.CharField(max_length=50,null=True,blank=True,default="ES3")
    t_alt_code_fi = models.CharField(max_length=50,null=True,blank=True,default="A")
    t_alt_code_tw = models.CharField(max_length=50,null=True,blank=True,default="B")
    t_alt_code_th = models.CharField(max_length=50,null=True,blank=True,default="C")
    t_alt_code_fi_a = models.CharField(max_length=50,null=True,blank=True,default="A")
    t_alt_code_tw_b = models.CharField(max_length=50,null=True,blank=True,default="B")
    t_alt_code_th_c = models.CharField(max_length=50,null=True,blank=True,default="C")



    def __str__(self):
        return f"{self.country_codes}"
    class Meta:
        verbose_name_plural = "countries"
class Connected(models.Model):
    language_name = models.CharField(max_length=50,default="SPANISH")
    language_code = models.CharField(max_length=20,default="IT")
    country_names = models.CharField(max_length=50,default="SPANISH")
    country_codes = models.CharField(max_length = 40,default="IT")
    cookie_set =  models.CharField(max_length=100,default="vvf")
    connected_languages = models.CharField(max_length=100,null=True,blank=True,default="SPANISH")
    text = models.TextField(default="Add text",null=True,blank=True)
    translation_language = models.CharField(max_length=50,null=True,blank=True,default="English")
    translation_language_second = models.CharField(max_length=50,null=True,blank=True,default="Us-engilsh")
    translation_language_third = models.CharField(max_length=50,null=True,blank=True,default="indian-english")
    t_code_fi = models.CharField(max_length=50,null=True,blank=True,default="ES1")
    t_code_tw = models.CharField(max_length=50,null=True,blank=True,default="ES2")
    t_code_th = models.CharField(max_length=50,null=True,blank=True,default="ES3")
    def __str__(self):
        return f"{self.country_codes}"
    class Meta:
        verbose_name_plural = "connected"
class FormDetails(models.Model):
    job_number = models.CharField(max_length=30)
    item_number = models.CharField(max_length=120)
    varient_code = models.CharField(max_length=50)

    def __str__(self):
        return  f"{self.varient_code}"
    class Meta:
        verbose_name = "Form Details"

class Translation(models.Model):
    text = models.TextField()
    translation_language = models.CharField(max_length=30,default="English")

    def __str__(self):
        return f"{self.text}"
