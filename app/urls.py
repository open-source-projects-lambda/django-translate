from django.urls import path
from .views  import IndexView,FormRegistration,TranslationDetailView,TranslationView,DetailView,ConnectedView

urlpatterns = [
    path('',IndexView.as_view(),name='index'),
    path('formdata',FormRegistration.as_view(),name="form"),
    path('posts/transview',TranslationView.as_view(),name="Translation"),
    path('trans/<pk>',TranslationDetailView.as_view(),name="Translation"),
    path('transview',TranslationDetailView.as_view()),
    path('blo/<int:pk>',DetailView.as_view(),name="detail"),
    path('blo/<int:pk',ConnectedView.as_view(),name="connected")
]
